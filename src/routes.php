<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    try {
        $sql = 'SELECT person.*, location.*, cnt_contact, cnt_meeting
                FROM person
                LEFT JOIN (
                  SELECT id_person, COUNT(*) AS cnt_contact
                  FROM contact
                  GROUP BY id_person
                ) AS contacts
                ON person.id_person = contacts.id_person
                LEFT JOIN (
                  SELECT id_person, COUNT(*) AS cnt_meeting
                  FROM person_meeting
                  GROUP BY id_person
                ) AS meetings
                ON person.id_person = meetings.id_person
                LEFT JOIN location
                USING(id_location)';
        $search = $request->getQueryParam('search');
        if($search) {
            $stmt = $this->db->prepare($sql . 'WHERE 
                                    first_name ILIKE :s OR
                                    last_name ILIKE :s OR 
                                    nickname ILIKE :s
                                  ORDER BY last_name');
            $stmt->bindValue(':s', '%' . $search . '%');
            $stmt->execute();
        } else {

            $stmt = $this->db->query($sql . 'ORDER BY last_name');
        }
        $tplVars['persons'] = $stmt->fetchAll();
        return $this->view->render($response, 'persons.latte', $tplVars);
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('index');

$app->get('/create-person', function (Request $request, Response $response, $args) {
    $tplVars['form'] = [
        'nn' => '',
        'ln' => '',
        'fn' => '',
        'bd' => '',
        'g' => 'unknown',
        'h' => '170'
    ];
    return $this->view->render(
        $response,
        'create-person.latte',
        $tplVars
    );
})->setName('createPerson');

$app->post('/create-person', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if(!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])) {
        try {
            $stmt = $this->db->prepare('INSERT INTO person
                           (birth_day, height, gender, first_name, last_name, nickname)
                           VALUES (:bd, :h, :g, :fn, :ln, :nn)');

            $stmt->bindValue(':bd', empty($data['bd']) ? null : $data['bd']);
            $stmt->bindValue(':h', empty($data['h']) ? null : $data['h']);
            $stmt->bindValue(':g', empty($data['g']) ? null : $data['g']);

            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->execute();

            return $response->withHeader('Location',
                $this->router->pathFor('index'));
        } catch (Exception $e) {
            if($e->getCode() == 23505) {

                $tplVars['error'] = 'This record already exist.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'create-person.latte', $tplVars);
            } else {

                $this->logger->error($e->getMessage());
                die($e->getMessage());
            }
        }
    } else {
        $tplVars['error'] = 'Add all required data.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'create-person.latte', $tplVars);
    }
});

function load_locations(PDO $db) {
    try {
        $stmt = $db->query('SELECT * FROM location ORDER BY city');
        return $stmt->fetchAll();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
}


$app->get('/create-person-address', function (Request $request, Response $response, $args) {
    $tplVars['form'] = [
        'nn' => '',
        'ln' => '',
        'fn' => '',
        'bd' => '',
        'g' => 'unknown',
        'h' => '170',

        'c' => '',
        'sna' => '',
        'snu' => '',
        'zip' => '',
        'ct' => '',

        'id_location' => ''
    ];

    $tplVars['locations'] = load_locations($this->db);

    return $this->view->render(
        $response,
        'create-person-address.latte',
        $tplVars
    );
})->setName('createPersonAddress');

$app->post('/create-person-address', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();

    $tplVars['locations'] = load_locations($this->db);

    if(!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])) {
        try {
            $id_location = null;

            $this->db->beginTransaction();

            if(!empty($data['id_location'])) {

                $id_location = $data['id_location'];
            } elseif(!empty($data['c'])) {

                $stmt = $this->db->prepare('INSERT INTO location
                                            (city, street_number)
                                            VALUES (:c, :snu)');
                $stmt->bindValue(':c', $data['c']);
                $stmt->bindValue(':snu', empty($data['snu']) ? null : $data['snu']);
                $stmt->execute();

                $id_location = $this->db->lastInsertId('location_id_location_seq');
            }

            $stmt = $this->db->prepare('INSERT INTO person
                           (id_location, birth_day, height, gender, first_name, last_name, nickname)
                           VALUES (:idl, :bd, :h, :g, :fn, :ln, :nn)');

            $stmt->bindValue(':idl', $id_location);
            $stmt->bindValue(':bd', empty($data['bd']) ? null : $data['bd']);
            $stmt->bindValue(':h', empty($data['h']) ? null : $data['h']);
            $stmt->bindValue(':g', empty($data['g']) ? null : $data['g']);

            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->execute();

            $this->db->commit();


            return $response->withHeader('Location',
                $this->router->pathFor('index'));
        } catch (Exception $e) {

            $this->db->rollback();

            if($e->getCode() == 23505) {

                $tplVars['error'] = 'This record exist.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'create-person-address.latte', $tplVars);
            } else {

                $this->logger->error($e->getMessage());
                die($e->getMessage());
            }
        }
    } else {
        $tplVars['error'] = 'Add all required data.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'create-person-address.latte', $tplVars);
    }
});

$app->get('/edit', function (Request $request, Response $response, $args) {
    // read GET data
    $params = $request->getQueryParams();
    try {
        $stmt = $this->db->prepare("SELECT * FROM person WHERE id_person = :id");
        $stmt->bindValue(":id", $params["id"]);
        $stmt->execute();

        $person = $stmt->fetch();
        if ($person) {
            $tplVars["form"] = $person;
        }
        else {
            $tplVars["warning"] = "Person not found.";
        }
    } catch(Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
    return $this->view->render($response, 'edit-person.latte', $tplVars);
})->setName('edit');

$app->post('/edited', function (Request $request, Response $response, $args){
    $id = $request->getQueryParam('id');
    function checkSQLDate($s) {
        $reg = "/^\d{4}-\d{2}-\d{2}$/";
        return preg_match($reg, $s);
    }

    $input = $request->getParsedBody();
    if (empty($input['first_name']) || empty($input['last_name']) || empty($input['nickname']) || (!empty($input['birth_day']) && !checkSQLDate($input['birth_day']))) {
        $tplVars['warning'] = 'Input required data';
        $tplVars['form'] = $input;
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE person SET first_name = :fn, 
                                      last_name = :ln, nickname = :nn, birth_day = :bd, height = :h WHERE id_person = :id");

            $birth_day = !empty($input['birth_day']) ? $input['birth_day'] : null;
            $height = !empty($input['height']) ? $input['height'] : null;

            $stmt->bindValue(":fn", $input['first_name']);
            $stmt->bindValue(":ln", $input['last_name']);
            $stmt->bindValue(":nn", $input['nickname']);
            $stmt->bindValue(":bd", $birth_day);
            $stmt->bindValue(":h", $height);
            $stmt->bindValue(":id", $id);

            $stmt->execute();

            return $response->withHeader('Location', $this->router->pathFor('index'));
        } catch(Exception $e) {
            $this->logger->error($e->getMessage());
            exit($e->getMessage());
        }
    }
    return $this->view->render($response, 'edit-person.latte', $tplVars);
})->setName('edited');

$app->post('/delete', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :idp");
        $stmt->bindValue(":idp", $input["id_person"]);
        $stmt->execute();
    } catch(Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('delete');

$app->get('/meeting', function (Request $request, Response $response, $args){
    try {
        $stmt = $this->db->query('SELECT * FROM meeting');
        $tplVars['meeting'] = $stmt->fetchAll();


        //$stmt = $this->db->prepare('SELECT * FROM meeting ORDER BY id_meeting');
        //$stmt->fetchAll();
        //$tplVars['meeting'] = $stmt->execute();

        /*$stmt = $this->db->prepare('SELECT * FROM location ORDER BY city');
        $stmt->fetchAll();
        $tplVars['location'] = $stmt->execute(); */

        return $this->view->render($response, 'meeting.latte', $tplVars);
    } catch(Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
})->setName('meeting');

$app->post('/createMeeting', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();

    if(!empty($data['start']) && !empty($data['duration']) && !empty($data['description'])) {
        try {
            $stmt = $this->db->prepare('INSERT INTO meeting (start, duration, description, id_location) VALUES (:start, :duration, :description, :location)');
            $stmt->bindValue(':start', $data['start']);
            $stmt->bindValue(':duration', $data['duration']);
            $stmt->bindValue(':description', $data['description']);
            $stmt->bindValue(':location', $data['location']);
            $stmt->execute();

            return $response->withHeader('Location',
                $this->router->pathFor('meeting'));
        } catch (Exception $e) {

            //$this->db->rollback();

            if($e->getCode() == 23505) {

                $tplVars['error'] = 'This record exist.';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'create-meeting.latte', $tplVars);
            } else {

                $this->logger->error($e->getMessage());
                exit($e->getMessage());
            }
        }
    } else {
        $tplVars['error'] = 'Add all required data.';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'create-meeting.latte', $tplVars);
    }
})->setName('createMeeting');

$app->get('/createMeeting', function (Request $request, Response $response, $args) {

    $tplVars['form'] = [
        'start' => '',
        'duration' => '',
        'location' => '',
        'description' => 'description',
    ];
    //$stmt = $this->db->prepare('SELECT * FROM location ORDER BY city');
    //$tplVars['location'] = $stmt;

    $stmt= $this->db->query('SELECT * FROM location ');
    $tplVars['location'] = $stmt->fetchAll();

    return $this->view->render($response, 'create-meeting.latte', $tplVars);
});

$app->get('/createContact', function (Request $request, Response $response){
        $id_contact = $request->getQueryParam('id_contact');

        $tplVars['contact'] = $this->db->query('SELECT first_name, last_name, name, contact FROM person RIGHT JOIN 
                                (SELECT * FROM contact LEFT JOIN contact_type  
                                ON contact.id_contact_type = contact_type.id_contact_type) AS contype
                                ON contype.id_person = person.id_person');
        $tplVars['form'] = ['contact'=> ''];

        $tplVars['id_contact'] = $id_contact;
        return $this->view->render($response, 'create-contact.latte', $tplVars);
})->setName('createContact');

$app->post('/createContact', function (Request $request, Response $response){
    $id = $request->getParsedBody();

    $id_contact = $request->getQueryParam('id_contact');

    $stmt = $this->db->prepare('INSERT INTO contact (id_contact_type, contact)
                                VALUES (:idc, :contact)');
    $stmt->bindValue(':idc', $id_contact);
    $stmt->bindValue(':contact', $id['contact']);
    $stmt->execute();
    return $response->withHeader('Location', $this->router->pathFor('persons'));
})->setName('createContact');

$app->get('/editContact', function (Request $request, Response $response){


    try {
        $id = $request->getParsedBody('id_contact');
        $contact = $request->getQueryParam('id_contact');
        $stmt = $this->db->query('SELECT * FROM contact WHERE id_contact = :id_contact');
        $stmt->bindValue(':id_contact', $contact);
        $stmt->fetch();
        $stmt = $this->db–>query('SELECT * FROM contact WHERE id_contact_type = :idc');
        $stmt->bindValue('idc', $id);
        $stmt->execute();
        $con = $stmt->fetchAll();


        if ($con) {
            $tplVars['form'] = $con;
        } else {
            $tplVars['warning'] = "Contact not found";
        }
        }catch (Exception $e){
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
    return $this->view->render($response, 'edit-contact.latte', $tplVars);
})->setName('editContact');

$app->post('/editContact', function (Request $request, Response $response){
    $id = $request->getParsedBody('id_contact');

    $id_contact = $request->getQueryParam('id_contact');

    $stmt = $this->db->prepare('UPDATE contact SET (id_contact_type, contact)
                                VALUES (:idc, :contact)');
    $stmt->bindValue(':idc', $id_contact);
    $stmt->bindValue(':contact', $id['contact']);
    $stmt->execute();
    return $response->withHeader('Location', $this->router->pathFor('persons'));
        return $this->view->render($response, 'edit-contact.latte', $tplVars);
})->setName('editContact');

$app->get('/meetingInfo', function (Request $request, Response $response){
    try {
        $id_meeting = $request->getQueryParam('id_meeting');

        $stmt = $this->db->prepare('SELECT * FROM meeting WHERE id_meeting = :id');
        $stmt->bindValue(':id', $id_meeting);
        $stmt->execute();
        $tplVars['meeting'] = $stmt->fetchAll();

        $stmt = $this->db->prepare('SELECT * FROM location WHERE id_location = :idl');
        $stmt->bindValue('idl', $tplVars['meeting']['id_location']);
        $stmt->execute();
        $location = $stmt->fetchAll();
        $tplVars['location'] = $location['city'] . $location ['street_name'] . $location['street_number'];

        $stmt = $this->db->prepare('SELECT id_person, first_name, last_name FROM person JOIN (SELECT id_person FROM meeting
                                  JOIN person_meeting USING(id_meeting) WHERE id_meeting = :idm) AS person USING(id_person)');

        $stmt->bindValue(':idm', $id_meeting);
        $stmt->execute();
        $tplVars['people'] = $stmt->fetchAll();

        return $this->view->render($response, 'meeting-info.latte', $tplVars);

    }catch (Exception $e){
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
})->setName('meetingInfo');

function load_location($db)
{
    try {
        $stmt = $db->query('SELECT * FROM location ORDER BY city');
        return $stmt->fetchAll();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
}

$app->get('/editMeeting', function (Request $request, Response $response){

    try {
        $id_meeting = $request->getQueryParams('id');
        $stmt = $this->db->prepare("SELECT * FROM meeting WHERE id_meeting = :id");
        $stmt->bindValue(":id", $id_meeting['id']);
        $stmt->execute();

        $meeting = $stmt->fetch();

    } catch(Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
    $tplVars['form'] = [
        'start' => $meeting['start'],
        'duration' => $meeting['duration'],
        'description' => $meeting['description'],
        'id_location' => $meeting['id_location']
    ];
    $tplVars['id_meeting'] = $id_meeting;
    $tplVars['location'] = load_location($this->db);
    return $this->view->render($response, 'edit-meeting.latte', $tplVars);
})->setName('editMeeting');

$app->post('/editMeeting', function (Request $request, Response $response, $args){
    $meeting = $request->getParsedBody();
    $id_meeting = $request->getQueryParams('id');

    $stmt = $this->db->prepare('UPDATE meeting SET start = :start,
                               description = :description, duration = :duration,
                               id_location = :id_location
                               WHERE id_meeting = :id');

    $stmt->bindValue(':start', $meeting['start']);
    $stmt->bindValue(':duration', $meeting['duration']);
    $stmt->bindValue(':description', $meeting['description']);
    $stmt->bindValue(':id_location', $meeting['id_location']);

    $stmt->bindValue(':id', $meeting['id']);
    $stmt->execute();
    return $response->withHeader('Location', $this->router->pathFor('meeting'));

})->setName('editMeeting');

$app->post('/deleteMeeting', function (Request $request, Response $response) {
    try {
        $id_meeting = $request->getQueryParam('id_meeting');

        $stmt = $this->db->prepare('DELETE FROM person_meeting WHERE id_meeting = :id_meeting');
        $stmt->bindValue(':id_meeting', $id_meeting);
        $stmt->execute();


        $stmt = $this->db->prepare('DELETE FROM meeting WHERE id_meeting = :id_meeting');
        $stmt->bindValue(':id_meeting', $id_meeting);
        $stmt->execute();

        return $response->withHeader(
            'Location', $this->router->pathFor('meeting')
            . '?succ=Meeting_deleted_successfully'
        );
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('deleteMeeting');

$app->get('/addPersonMeeting', function(Request $request, Response $response){
    try{
        $tplVars['id_meeting'] = $request->getQueryParams('id_meeting');

        $tplVars['form'] = ['person' => ''];
        $tplVars['people'] = $this->db->query('SELECT * FROM person ORDER BY id_person');

        //$tplVars->fetchAll();

        return $this->view->render($response, 'add-person-meeting.latte', $tplVars);
    } catch (Exception $e){
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
})->setName('addPersonMeeting');

$app->post('/addPersonMeeting', function(Request $request, Response $response){
    $id_meeting = $request->getQueryParam('id_meeting');
    $id_person = $request->getQueryParam('id_person');



            $stmt = $this->db->prepare('INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)'); //select * from person_meeting...
            $stmt->bindValue(':id_person', $id_person);
            $stmt->bindValue(':id_meeting', $id_meeting);

            $stmt->fetchAll();

            return $response->withHeader('Location',
                $this->router->pathFor('meeting'));

})->setName('addPersonMeeting');

$app->get('/location', function(Request $request, Response $response){

    $stmt = $this->db->query('SELECT * FROM location ORDER BY city');
    $tplVars['locations'] = $stmt->fetchAll();
    return $this->view->render($response,'location.latte', $tplVars);
})->setName('location');

$app->get('/createLocation', function(Request $request, Response $response){
    $tplVars['l'] = [
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',
        'country' => '',
        'name' => '',
        'latitude' => '',
        'longitude' => '',
    ];

    return $this->view->render($response, 'create-location.latte',$tplVars);
})->setName('createLocation');

$app->post('/createLocation', function (Request $request, Response $response){
    $location = $request->getParsedBody();

    $stmt = $this->db->prepare('INSERT INTO location (city, street_name, street_number,
                              zip, country, name, latitude, longitude) VALUES (:city, :street_name, :street_number,
                              :zip, :country, :name, :latitude, :longitude)');
        $stmt->bindValue(':city', $location['city']);
        $stmt->bindValue(':street_name', $location['street_name']);
        $stmt->bindValue(':street_number', $location['street_number']);
        $stmt->bindValue(':zip', $location['zip']);
        $stmt->bindValue(':country', $location['country']);
        $stmt->bindValue(':name', $location['name']);
        $stmt->bindValue(':latitude', $location['latitude']);
        $stmt->bindValue(':longitude', $location['longitude']);

        $stmt->execute();
        return $response->withHeader('Location', $this->router->pathFor('location'));

})->setName('createLocation');

$app->post('/deleteLocation', function(Request $request, Response $response){
    /*try {
        $id = $request->getParsedBody();

        $stmt = $this->db->prepare('DELETE FROM location WHERE id_location = :id');
        $stmt->bindValue(':id', $id["id_location"]);

        $stmt->execute();

        return $response->withHeader('Location', $this->router->pathFor('location'));
        }catch (Exception $e){
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    } */
    try {
        $id_location = $request->getQueryParam('id_location');

        $stmt = $this->db->prepare('DELETE FROM location WHERE id_location = :id_location');
        $stmt->bindValue(':id_location', $id_location);
        $stmt->execute();


        return $response->withHeader(
            'Location', $this->router->pathFor('location')
            . '?succ=Location_deleted_successfully'
        );
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('locationDelete');

$app->get('/contact', function (Request $request, Response $response){

        $stmt = $this->db->query('SELECT first_name, last_name, name, contact FROM person RIGHT JOIN 
                                (SELECT * FROM contact LEFT JOIN contact_type  
                                ON contact.id_contact_type = contact_type.id_contact_type) AS contype
                                ON contype.id_person = person.id_person');
    $tplVars['contact'] = $stmt->fetchAll();
    return $this->view->render($response, 'contact.latte', $tplVars);
})->setName('contact');

$app->post('/deleteContact', function (Request $request, Response $response){
    try {
        $id_contact= $request->getQueryParam('id_contact');

        $stmt = $this->db->prepare('DELETE FROM contact WHERE id_contact = :id_contact');
        $stmt->bindValue(':id_contact', $id_contact);
        $stmt->execute();


        return $response->withHeader(
            'Location', $this->router->pathFor('contact')
            . '?succ=Contact_deleted_successfully'
        );
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('deleteContact');

$app->get('/editLocation', function (Request $request, Response $response){

    try {
        $params = $request->getQueryParam('id_location');
        $stmt = $this->db->prepare("SELECT * FROM location WHERE id_location = :id_location");
        $stmt->bindValue(":id_location", $params);
        $stmt->execute();

        $location = $stmt->fetch();
        if ($location) {
            $tplVars["id_location"] = $location;
        }
        else {
            $tplVars["warning"] = "Person not found.";
        }
    } catch(Exception $e) {
        $this->logger->error($e->getMessage());
        exit($e->getMessage());
    }
    return $this->view->render($response, 'edit-location.latte', $tplVars);
})->setName('editLocation');

$app->post('/editLocation', function (Request $request, Response $response){
    $id = $request->getQueryParam('id_location');

    $input = $request->getParsedBody();

        try {
            $stmt = $this->db->prepare("UPDATE location SET (city = :city, street_name = :street_name, 
                                        street_number = :street_number, zip = :zip, country = :country, name = :name,
                                        latitude =:latitude, longitude = :longitude)WHERE id_location = :id_location");

            $stmt->bindValue(":city", $input['city']);
            $stmt->bindValue(":street_name", $input['street_name']);
            $stmt->bindValue(":street_number", $input['street_number']);
            $stmt->bindValue(":zip", $input['zip']);
            $stmt->bindValue(":country", $input['country']);
            $stmt->bindValue(":name", $input['name']);
            $stmt->bindValue(":latitude", $input['latitude']);
            $stmt->bindValue(":longitude", $input['longitude']);
            $stmt->bindValue(":id_location", $id['id_location']);

            $stmt->execute();

            return $response->withHeader('Location', $this->router->pathFor('location'));
        } catch(Exception $e) {
            $this->logger->error($e->getMessage());
            exit($e->getMessage());
        }

    return $this->view->render($response, 'edit-location.latte', $tplVars);
})->setName('editLocation');